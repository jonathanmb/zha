# Setup
This folder contains the inital files required to seed

## Instructions
Copy most/all the files from the setup folder into the root of the pol folder and modify them.

## File listing
* config\servers.cfg - Server listings.
* pol.cfg - Main pol configuration file. Change the UoDataFileRoot variable.
* POLHook.cfg - The configuration for the pol hook settings. Change the log directory.
* data\ - A copy of a fully populated old ZHA world file.
* data-empty\ - A blank world file for quick boot up script testing.
