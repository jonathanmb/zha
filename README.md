# README #
### What is this repository for? ###

* Zulu Hotel Australia 2016
* Current version running on POL0.95

### How do I get set up? ###

* Scripts should just need to be downloaded (suggest sourcetree).
* Change the two executables from .exe.RENAME to just .exe (/pol.exe and /scripts/ecompile.exe)
* Rename /scripts/ecompile.cfg.example to ecompile.cfg and edit it to match your OS folder structure.
* Copy setup/pol.cfg to / and edit it to match your UO directory.
* Copy setup/POLHook.cfg to / and edit it to match your UO directory.
* Copy setup/config/servers.cfg to /config/ and edit the ips if required (Shouldn't be needed).
* Copy setup/data or setup/data-empty to /data to initally set up the world.
* Open a CMD prompt and navigate to the scripts folder and run ecompile.bat.
* Once ecompile finishes go back up a level and run pol.exe
* test change.

### Development environemnt ###
You can use the /dev folder to place all your development environment config files, programs and other scripts that wont be checked in.
A recommended development tool (IDE) to use for server scripting is [Visual Studio Code](https://www.visualstudio.com/products/code-vs.aspx) which is a light weight editor with git support.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jonathan Morland-Barrett (Dev GIB) - jmorland.barrett@gmail.com
* Craig Bruce (Harlz) - craigbruce32@gmail.com
* Murray Hipper (Seravy) - seravy@gmail.com